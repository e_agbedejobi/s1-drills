"""
Functions 01: Strings

This module will help you improve your understanding of functions and strings.
Each function below contains a test in its docstring.

Learn more about docstrings: https://docs.python.org/3/library/doctest.html

Instructions
------------

1. Read the description of each function.
2. Start coding your solution.
3. Pass all the tests!

To run the tests, simply execute this python script from the CLI.
Always solve 1 problem at a time.
"""


def capitalize(word: str) -> str:
    """
    Capitalize a given word and return it.

    >>> capitalize('giraffe')
    'Giraffe'
    """
    # your code here


def lowercase(word: str) -> str:
    """
    Return a given word in all lowercase letters.

    >>> lowercase('CHIMPANZEE')
    'chimpanzee'
    >>> lowercase('AlliGatoR')
    'alligator'
    """
    # your code here


def uppercase(word: str) -> str:
    """
    Return a given word in all uppercase letters.
    >>> uppercase('crocodile')
    'CROCODILE'
    >>> uppercase('ElephaNt')
    'ELEPHANT'
    """
    # your code here


def censor(word: str) -> str:
    """
    Take the first letter of a word and replace all occurrences thereafter with a '-'.
    Do not change the first letter of the word, only the following appearances.

    >>> censor('turtle')
    'tur-le'
    >>> censor('giggle')
    'gi--le'
    >>> censor('dragon')
    'dragon'
    """
    # Your code here


def flip(word: str) -> str:
    """
    Given a string, return it in backwards order.
    >>> flip('rhino')
    'onihr'
    >>> flip('red panda')
    'adnap der'
    """
    # your code here


def get_vowel_count(text: str) -> int:
    """
    Given a string, return a count of all the vowels within.
    >>> get_vowel_count('mississippi')
    4
    >>> get_vowel_count('oranges')
    3
    >>> get_vowel_count('pineapple')
    4
    """
    # your code here


def is_palindrome(phrase: str) -> bool:
    """
    Given a word, return true if its a palindrome, false if not.
    >>> is_palindrome('color')
    False
    >>> is_palindrome('level')
    True
    >>> is_palindrome('my gym')
    True
    """
    # your code here


def first_last(word: str) -> str:
    """
    Return the first and last letter of a string.

    >>> first_last('turtle')
    't e'
    >>> first_last('peacock')
    'p k'
    """
    # Your code here


def letter_swap(word1: str, word2: str) -> str:
    """
    Given two words, swap the first letter of each and return the new string.

    >>> letter_swap('mouse', 'cat')
    'couse mat'
    >>> letter_swap('koala', 'kangaroo')
    'koala kangaroo'
    >>> letter_swap('sloth', 'horse')
    'hloth sorse'
    """
    # Your code here


def alpha_len(word: str) -> int:
    """
    Implement the len() function ...with a twist.
    Return the total number of letters in a given string, but do not count whitespace.
    >>> alpha_len('mouse')
    5
    >>> alpha_len('cat and mouse')
    11
    """
    # your code here


def capital_count(text: str) -> int:
    """
    Given a string of text, count the total number of capitalized letters in it.
    >>> capital_count('I am a Man that Stands Above all other Men.')
    4
    >>> capital_count('I like Charles Van McDonald as a hAcKeRZ')
    9
    """
    # your code here


def most_common_letter(text: str) -> str:
    """
    Given a string, return the most common letter.
    >>> most_common_letter('Green eggs and ham, sam I am')
    'a'
    >>> most_common_letter('long socks are comfortable')
    'o'
    """
    # your code here


if __name__ == '__main__':
    import doctest
    doctest.testmod()
