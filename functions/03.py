"""
Functions 03: Collections

This module will help you improve your understanding of functions and collections.
Collections are objects such as lists, dictionaries and tuples.
Each function below contains a test in its docstring.

Learn more about docstrings: https://docs.python.org/3/library/doctest.html

Instructions
------------

1. Read the description of each function.
2. Start coding your solution.
3. Pass all the tests!

To run the tests, simply execute this python script from the CLI.
Always solve 1 problem at a time.
"""
from typing import List
from resources.texts import *


def sentence_count(text: str) -> int:
    """
    Given a block of text, return the total number of sentences in it.

    >>> sentence_count(BACK_TO_THE_FUTURE_1)
    5
    """
    # your code here


def shortest_words(text: str, amount: int) -> list:
    """
    Given a string of words, return the shortest words.
    >>> shortest_words('i can count to five', 2)
    ['i', 'to']
    >>> shortest_words('green eggs taste amazing with salt', 3)
    ['eggs', 'with', 'salt']
    """
    # your code here


def word_space_analyzer(phrase: str) -> dict:
    """
    Given a string, count the total number of words in it and a count of the the number spaces.
    >>> word_space_analyzer('monkeys like bananas')
    {'words': 3, 'spaces': 2}
    >>> word_space_analyzer(BACK_TO_THE_FUTURE_3)
    {'words': 94, 'spaces': 86}
    >>> word_space_analyzer(BACK_TO_THE_FUTURE_4)
    {'words': 160, 'spaces': 149}
    """
    # your code here


def find_duplicates(phrase: str) -> list:
    """Given a string, return a sorted list of words that are repeated.
    >>> find_duplicates('the box is in a box')
    ['box']
    >>> find_duplicates('the house has a room with a box with room for a box')
    ['a', 'box', 'room', 'with']
    """
    # your code here


def ordered_merge(list1: List[int], list2: List[int]) -> list:
    """
    Given 2 lists of numbers, combine them into 1 and sort from lowest to highest.
    >>> ordered_merge([3, 6, 9], [4, 8])
    [3, 4, 6, 8, 9]
    >>> ordered_merge([2, 4, 6], [1, 2, 3])
    [1, 2, 2, 3, 4, 6]
    """
    # your code here


def text_counter(text: str) -> dict:
    """
    Return a count of all letters in a string.
    >>> text_counter('lovely eyes')
    {'l': 2, 'o': 1, 'v': 1, 'e': 3, 'y': 2, 's': 1}
    """
    # your code here


if __name__ == '__main__':
    import doctest
    doctest.testmod()
