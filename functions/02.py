"""
Functions 02: Numbers

This module will help you improve your understanding of functions and numbers.
Each function below contains a test in its docstring.

Learn more about docstrings: https://docs.python.org/3/library/doctest.html

Instructions
------------

1. Read the description of each function.
2. Start coding your solution.
3. Pass all the tests!

To run the tests, simply execute this python script from the CLI.
Always solve 1 problem at a time.
"""


def word_count(phrase: str) -> int:
    """
    Given a string, count the total number of words in it.
    >>> word_count('monkeys like bananas')
    3
    """
    # your code here


def is_odd(num: int) -> bool:
    """
    Given a number, return whether its odd.
    >>> is_odd(80)
    False
    >>> is_odd(543)
    True
    """
    # your code here


def modulo(num: int, divisor: int) -> int:
    """
    Given a number num and a divisor, return the remainder of their division.
    >>> modulo(7, 3)
    1
    """
    # your code here


def is_prime(num: int) -> bool:
    """
    Given a number, return true if its a prime number, false if not.
    Learn more: https://byjus.com/maths/prime-numbers/

    >>> is_prime(3)
    True
    >>> is_prime(10)
    False
    """
    # your code here


def get_highest(nums: str) -> int:
    """
    Given a string of numbers, return the highest number.
    >>> get_highest('32 7 160 42')
    160
    """
    # your code here


def fibonacci_up_to(limit: int) -> int:
    """
    Given a number num, return a count of fibonacci numbers up to the limit.
    Learn more: https://byjus.com/maths/fibonacci-sequence/

    Example:
        fibonacci_count(10) means start the fibonacci sequence stopping at 10
        0, 1, 1, 2, 3, 5, 8 -> the next nubmer is 13, so you must stop at 8

        There are 7 fibonancci numbers up to 10

    >>> fibonacci_up_to(10)
    7
    >>> fibonacci_up_to(3)
    4
    """
    # your code here


def factorial(num: int) -> int:
    """
    Given a number, return its factorial.
    >>> factorial(3)
    6
    >>> factorial(5)
    120
    """
    # your code here


if __name__ == '__main__':
    import doctest
    doctest.testmod()
