# Python Drills

A collection of short exercises to practice and improve Python skills.

## Instructions

1. Download the code.
2. Open any script in your editor.
3. Run the script: `python3 path/to/script_name.py`

This will display the current test results for the module. Read the failures. 

One by one, make each test pass. Only work on one test at a time.
