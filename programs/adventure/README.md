# Choose Your Adventure

All adventure games descend from the long loved choose your own adventure books. These were books that would let you make choices to create your own story.

## Instructions

Your task is to do the same. Create an adventure story that allows the player to make choices. Your game should:

* have at least 2 endings
* allow the player to make at least 3 choices

Before you write any code, run your game by typing the following command to ensure everything's working:

```bash
python3 -m adventure.start
```
> Users of different operating systems may need to use `python` instead of `python3`.


## Getting Started

First, create a draft of your story. It can be about anything you like, but try to make it interesting so that players enjoy it! You can write it in comments or on paper to get you started. Outline some the player options as well.

Once you  have a basic story, there are some particularly useful tools you can use to make your game. Before you code, experiment with the items below to learn how they they might be helpful.

- variables: these will be incredibly useful to editing the story along the way.
- `input()`: Get player input from the terminal
- `logging`: Log all the player choices and the story
- `print()`: Print messages.
- string formatting 

Remember: run your game everytime you make changes! Always test that your code works before writing more code.

## Fun Ideas

Some optional things you might do to spice up your tale ...

- text colors: https://gist.github.com/rene-d/9e584a7dd2935d0f461904b9f2950007
- ascii art: https://www.asciiart.eu/
