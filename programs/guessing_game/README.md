# Guessing Game

Create a game that allows players to try to fill in the blanks to guess
a random word. Kinda like Hangman or Wheel of Fortune. The game rules:

![game play example](assets/example.gif)

1. Player gets a limited number of attempts.
2. Longer words should get more attempts.
3. Word must be chosen at random and is hidden from the player.
4. When the player guesses the next letter correctly, display it.
5. Wrong guesses are counted against remaining attempts.
6. If player runs out of attempts, reveal the answer.

For this challenge, configure a logger using the Python Logging
Cookbook as your guide:
https://docs.python.org/3/howto/logging-cookbook.html

Logging instructions:

- Use INFO logs to display game status info to the player, such as the state of the word, how many attempts are left, etc.
- Use DEBUG logs to monitor state change in the program to debug.log file. This would include logging parts of the code that change the state of the game.
- Use Error logs to record program errors.
